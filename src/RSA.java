import java.util.List;


@SuppressWarnings({"StatementWithEmptyBody", "SameParameterValue", "StringConcatenationInLoop", "WeakerAccess"})
public class RSA {
    RSA rsa;
    private long[] m;
    private long[] c;
    private long[] m_new;

    private long p;
    private long q;

    private long n;
    private long fi_n;

    private long e;
    private long d;

    // With random values
    RSA() {
        this(2, 15, 2, 17);
    }

    RSA(long m_min, long m_max, long prime_min, long prime_max) {
        long[] m_tmp;
        long p_tmp;
        long q_tmp;

        do {
            clear();
            m_tmp = new long[]{getRandomValue(m_min, m_max), getRandomValue(m_min, m_max)};
            p_tmp = getRandomPrime(prime_min, prime_max);
            q_tmp = getRandomPrime(prime_min, prime_max);
        } while (m_tmp[0] == m_tmp[1] || p_tmp == q_tmp);
        run(m_tmp, p_tmp, q_tmp);
    }

    RSA(long[] m, long p, long q) {
        run(m, p, q);
    }

    RSA run(long[] m, long p, long q) {
        clear();
        this.m = m;
        this.p = p;
        this.q = q;

        this.n = get_n(p, q);
        this.fi_n = get_fi_n(p, q);
        this.e = get_e(fi_n);
        this.d = get_d(fi_n, e);

        // encrypt
        c = crypt(this.m, this.e, this.n);
        // decrypt
        m_new = crypt(c, d, n);
        if (m[0] != m_new[0] || m[1] != m_new[1]) {
            clear();
            System.out.println("FAILURE: Values are too high.");
        }
        return this;
    }

    private long[] crypt(long[] message, long key, long n) {
        long[] tmp = new long[message.length];
        for (int i = 0; i < message.length; i++) {
            List<Long> m = PrimeFactorization.run(message[i]); // to avoid long.max_value we split our message
            PrimeFactorization.print();
            tmp[i] = 1;
            for (Long l : m)
                for (int k = 0; k < key; k++) { // pow with mod
                    tmp[i] *= l;
                    tmp[i] %= n;
                }
        }
        return tmp;
    }

    RSA print() {
        String s = "";
        if (m != null) {
            s = "m = <";
            for (long l : m) s += l + ",";
            s = s.substring(0, s.length() - 1);
            s += ">\np = " + p + ", q = " + q + "\nn = " + n + "\nfi(n) = " + fi_n + "\ne = " + e + "\nc = <";
            for (long l : c) s += l + ",";
            s = s.substring(0, s.length() - 1);
            s += ">" + "\nd = " + d + "\nm_new = <";
            for (long l : m_new) s += l + ",";
            s = s.substring(0, s.length() - 1) + ">";
        } else
            s += "No calculation so far.";
        System.out.println(s);
        return this;
    }

    // variables

    long get_n(long p, long q) {
        if (!isPrime(p) || !isPrime(q))
            throw new ArithmeticException("p and q needs to be prime.");
        return p * q;
    }

    long get_fi_n(long p, long q) {
        return (p - 1) * (q - 1);
    }

    @SuppressWarnings("unused")
    long get_fi_n(long n) {
        List<Long> tmp = getPQ(n);
        if (tmp.size() != 2)
            throw new ArithmeticException("n does not suit.");
        return get_fi_n(tmp.get(0), tmp.get(1));
    }

    List<Long> getPQ(long n) {
        List<Long> tmp = PrimeFactorization.run(n); // to avoid long.max_value we split our message
        return tmp;
    }

    long get_e(long fi_n) {
        long result = 1; // starts with 2
        while (get_ggt(++result, fi_n) != 1) ;
        return result;
    }

    long get_d(long fi_n, long e) {
        long result = 0;
        while (((e * ++result) - 1) % fi_n != 0) ;
        return result;
    }

    // helper

    long get_ggt(long a, long b) {
        if (a == 0) return b;
        while (b != 0)
            if (a > b) a = a - b;
            else b = b - a;
        return a;
    }

    long getRandomValue(long min, long max) {
        return min + (long) (Math.random() * ((max - min) + 1));
    }

    void clear() {
        m = null;
        c = null;
        m_new = null;
        p = 0;
        q = 0;
        n = 0;
        fi_n = 0;
        e = 0;
        d = 0;
    }

    // prime

    long getRandomPrime(long min, long max) {
        if (min < 2) min = 2;
        return nextPrime(getRandomValue(min, max));
    }

    @SuppressWarnings("StatementWithEmptyBody")
    long nextPrime(long i) {
        while (!isPrime(i++)) ;
        return --i;
    }

    boolean isPrime(long n) {
        for (int i = 2; i <= n / 2; i++)
            if (n % i == 0) return false;
        return true;
    }

    // for testing

    RSA generateTaskForPractice() {
        RSA rsa = new RSA(2, 15, 2, 17);
        do rsa = new RSA(2, 15, 2, 17); while (!rsa.goodForLearning());
        return rsa.print();
    }

    boolean goodForLearning() {
        boolean good = true;
        if (m[0] != m_new[0] || m[1] != m_new[1]) {
            System.out.println("ungeeignet, da m != m_new");
            good = false;
        }
        if (m[0] == c[0]) {
            System.out.println("ungeeignet, da m[0] == c[0]");
            good = false;
        }
        if (m[1] == c[1]) {
            System.out.println("ungeeignet, da m[1] == c[1]");
            good = false;
        }
        if (e == d) {
            System.out.println("ungeeignet, da e == d");
            good = false;
        }
        if (d > 20) {
            System.out.println("ungeeignet, da d > 20");
            good = false;
        }
        return good;
    }
}