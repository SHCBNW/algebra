import java.util.ArrayList;
import java.util.List;

@SuppressWarnings({"StringConcatenationInLoop", "SameParameterValue", "UnusedReturnValue", "StatementWithEmptyBody", "BooleanMethodIsAlwaysInverted", "unused"})
public class PrimeFactorization {

    private static long value;
    private static List<Long> list = new ArrayList<>();
    private static List<Long> prime = new ArrayList<>();

    public static void main(String[] args) {
        for (int i = 2; i < 100; i++) {
            run(i);
            print();
            // printPrime();
        }
    }

    public PrimeFactorization() {
    }

    public static List<Long> run(long i) {
        clear();
        value = i;
        list.add(i);
        while (!isPrime(getLastFromList())) {
            long[] tmp = split(getLastFromList());
            deleteLast();
            list.add(tmp[0]);
            list.add(tmp[1]);
        }
        if (getLastPrime() != getLastFromList()) prime.add(getLastFromList());
        return list;
    }

    private static long[] split(long i) {
        long prime = getLastPrime();
        if (i % prime != 0) {
            nextPrime();
            return split(i);
        } else
            return new long[]{prime, i / prime};
    }

    private static long nextPrime() {
        long i = getLastPrime() + 1;
        while (!isPrime(i++)) ;
        prime.add(--i);
        return i;
    }

    private static boolean isPrime(long n) {
        for (int i = 2; i <= n / 2; i++)
            if (n % i == 0) return false;
        return true;
    }

    private static long getLastPrime() {
        if (prime.size() == 0) prime.add((long) 2);
        return prime.get(prime.size() - 1);
    }

    private static long getLastFromList() {
        return list.get(list.size() - 1);
    }

    private static void deleteLast() {
        list.remove(list.size() - 1);
    }

    static void print() {
        String s = value + " = ";
        for (Long l : list) s += l + " * ";
        System.out.println(s.substring(0, s.length() - 3));
    }

    private static void printPrime() {
        String s = "";
        for (Long l : prime) s += l + ", ";
        System.out.println("Primes = (" + s.substring(0, s.length() - 2) + ")");
    }

    private static void clear() {
        value = 0;
        list.clear();
        prime.clear();
    }
}