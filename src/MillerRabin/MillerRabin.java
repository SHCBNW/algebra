package MillerRabin;

import java.util.ArrayList;
import java.util.List;

public class MillerRabin {


    public static void main(String[] args) {
        long p = 41;
        long[] b = {2, 3, 20, 25};

        // find s and d;
        // p-1 = 2^s * d
        long counter = 1, s = -1, pow;
        long pNew = p - 1;

        do {
            pow = (long) Math.pow(2, counter);
            if (pNew % pow == 0) s = counter;
            counter++;
        } while (pow < pNew);
        long d = pNew / counter;

        long elements = s + 1;
        List<List> allBases = new ArrayList<>();

        System.out.println(s);
        System.out.println(d);


    }

    List<Long> getBase(long b, long d, long size) {

        return null;
    }


    static void run(long[] b, long p) {
        if (!isPrime(p)) throw new ArithmeticException("p: " + p + " is not prime.");


    }

    static boolean isPrime(long n) {
        for (int i = 2; i <= n / 2; i++)
            if (n % i == 0) return false;
        return true;
    }
}
